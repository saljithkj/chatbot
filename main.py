# -*- coding: utf-8 -*-
import logging
import re

import telegram
from functools import wraps
from threading import Lock
from settings import*
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler, RegexHandler,ConversationHandler
import json 
from datetime import datetime

from db_wrapper import DBwrapper as sqlitedb
from telegram import TelegramObject
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, utils, keyboardbutton,ParseMode
from telegram.utils import helpers
__author__ = 'Rico'

BOT_TOKEN = "1392974453:AAHmjYoKU0frlyzNjHGWtH-DhQt9WlCzd1w"
LIST_OF_ADMINS = [1061660183]
CHECK_THIS_OUT = 'INVT'
BOT_SENDS = "\U0001F916 *Bot:*"
BOT_BROADCAST = "\U0001F916 *Bot (Broadcast):*"
STRANGER_SENDS = "\U0001F464:"
onchat=False
feed=False
servers=True



FIRST, SECOND ,GENDER, PHOTO, LOCATION, BIO,PGENDER,GENDERTYPE,AGE,FEED,BAN,UNBAN ,YGENDER,PGENDER,GBACK,THIRD,MALE,FEMALE,OTHER,PBACK,FOUR,FIVE= range(22)


logger = logging.getLogger(__name__)
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

if not re.match(r"[0-9]+:[a-zA-Z0-9\-_]+", BOT_TOKEN):
    logging.error("Bot token not correct - please check.")
    exit(1)

updater = Updater(token=BOT_TOKEN, use_context=True, workers=32)
dispatcher = updater.dispatcher
tg_bot = updater.bot
lock = Lock()

chatting_users = []
chatting_userstime = []
searching_users = []




reply_user_message_id = []




def restricted(func):
    @wraps(func)
    def wrapped(bot, update, *args, **kwargs):
        user_id = update.effective_user.id
        if user_id not in LIST_OF_ADMINS:
            logger.info("Unauthorized access denied for method '{}' for user {}.".format(
                func.__name__, user_id))
            return
        return func(bot, update, *args, **kwargs)
    return wrapped


def start(update,context):
    gen=False
    
    global servers
    if servers:
       servers=False
       if len(chatting_users) == 0 and len(searching_users) == 0:
          serverOn()

        
   
    # user_id = update.message.from_user.id
    user_id=0
    user=2
    try:
      user_id = update.effective_chat.id
      user = update.callback_query.message.chat
    except Exception as e:
      user_id = update.message.from_user.id
      user = update.message.from_user
   

    
    context.bot.send_message(1061660183,"🏉 Start :"+str(user.first_name) )
    db = sqlitedb.get_instance()
    reply_keyboard = [["🧶 Stop Searching 🧶"]]
    reply_keyboard1 = [["🧶 Left the Chat 🧶"]]
    # keyboard = [[InlineKeyboardButton("🧶 Stop Searching 🧶", callback_data='1')]]
    # keyboard = [[KeyboardButton("🧶 Stop Searching 🧶", callback_data='1')]]


  
    reply_markup = telegram.ReplyKeyboardMarkup(reply_keyboard, resize_keyboard=True)
    reply_markup1 = telegram.ReplyKeyboardMarkup(reply_keyboard1, resize_keyboard=True)
    context.bot.send_chat_action(chat_id=update.effective_chat.id,action=telegram.ChatAction.TYPING)
    





    if user_id in db.get_banned_users():
        context.bot.send_message(user_id, "{} You have been banned from using this bot for few days!".format(
            BOT_SENDS), parse_mode="Markdown")
        return

    
    
    if (user_id not in searching_users) and (user_already_chatting(user_id) == -1):
        
       
        if len(searching_users) > 0:
           
            if(2 == db.getPartnerGender(user_id)):
                match=True
                for idx, item in enumerate(searching_users):
                    if 2==db.getGender(item):
                       if db.getPartnerGender(item)==0 or db.getPartnerGender(item) == db.getGender(user_id):
                           if (recentchat_time(user_id,item)):
                               match=False
                               with lock:
                                    partner_id = searching_users[idx]
                                    db.delete_searching(searching_users[idx])
                                    del searching_users[idx]
                               chatting_users.append([user_id, partner_id])
                               chatting_users.append([partner_id, user_id])
                               db.add_chating(user_id,partner_id)
                               db.add_chating(partner_id,user_id)
                               MacthedMsg(update,context,partner_id,reply_markup1,"PF")     
                               break 
                        
                    else:
                        match=True
                if match:
                    searching_users.append(user_id)
                    db.add_searching(user_id)
                    context.bot.send_message(user_id,text="🥏 Searching for strangers! Please Wait 🥏", reply_markup=reply_markup)           
            elif (1== db.getPartnerGender(user_id) or 3 == db.getPartnerGender(user_id)):
                match=True
                for idx, item in enumerate(searching_users):
                    if (1==db.getGender(item) or 3==db.getGender(item)) :
                       
                        if db.getPartnerGender(item)==0 or db.getPartnerGender(item) == db.getGender(user_id):
                           if (recentchat_time(user_id,item)):
                               match=False
                               with lock:
                                    partner_id = searching_users[idx]
                                    db.delete_searching(searching_users[idx])
                                    del searching_users[idx]
                                    
                               chatting_users.append([user_id, partner_id])
                               chatting_users.append([partner_id, user_id])
                               db.add_chating(user_id,partner_id)
                               db.add_chating(partner_id,user_id)
                               MacthedMsg(update,context,partner_id,reply_markup1,"PMO")     
                               break 
                    else:
                        match=True
                if match:
                    searching_users.append(user_id)
                    db.add_searching(user_id)
                    context.bot.send_message(user_id,text="🥏 Searching for strangers! Please Wait 🥏", reply_markup=reply_markup)    
            
            elif (2 == db.getGender(user_id)):
                match=True
                for idx, item in enumerate(searching_users):
                    if 2==db.getPartnerGender(item):
                       if (recentchat_time(user_id,item)):
                           match=False
                           with lock:
                                partner_id = searching_users[idx]
                                db.delete_searching(searching_users[idx])
                                del searching_users[idx]
                           chatting_users.append([user_id, partner_id])
                           chatting_users.append([partner_id, user_id])
                           db.add_chating(user_id,partner_id)
                           db.add_chating(partner_id,user_id)
                           MacthedMsg(update,context,partner_id,reply_markup1,"GF")     
                           break 
                    else:
                        match=True
                if match:
                   if len(searching_users) > 0:
                       matchs=True
                       for idx, item in enumerate(searching_users):
                           if 0==db.getPartnerGender(item):
                              if (recentchat_time(user_id,item)):
                                  matchs=False
                                  with lock:
                                       partner_id = searching_users[idx]
                                       db.delete_searching(searching_users[idx])
                                       del searching_users[idx]
                                  chatting_users.append([user_id, partner_id])
                                  chatting_users.append([partner_id, user_id])
                                  db.add_chating(user_id,partner_id)
                                  db.add_chating(partner_id,user_id)
                                  MacthedMsg(update,context,partner_id,reply_markup1,"GF")     
                                  break 
                           else:
                                matchs=True
                       if matchs:
                                  
                                  searching_users.append(user_id)
                                  db.add_searching(user_id)
                                  context.bot.send_message(user_id,text="🥏 Searching for strangers! Please Wait 🥏", reply_markup=reply_markup)  
                   
               
            elif (1== db.getGender(user_id) or 3 == db.getGender(user_id)):    
                match=True
                for idx, item in enumerate(searching_users):
                    if 0==db.getPartnerGender(item) or db.getPartnerGender(item)==db.getGender(user_id):
                       if (recentchat_time(user_id,item)):
                           match=False
                           with lock:
                                partner_id = searching_users[idx]
                                db.delete_searching(searching_users[idx])
                                del searching_users[idx]
                           chatting_users.append([user_id, partner_id])
                           chatting_users.append([partner_id, user_id])
                           db.add_chating(user_id,partner_id)
                           db.add_chating(partner_id,user_id)
                           MacthedMsg(update,context,partner_id,reply_markup1,"GMO")
                       break 
                    else:
                        match=True 
                if match:
                    searching_users.append(user_id)
                    db.add_searching(user_id)
                    
                    context.bot.send_message(user_id,
                             "🥏 Searching for strangers! Please Wait 🥏", reply_markup=reply_markup)
            else:
                gen=True
                
               
        else:
            
            
            searching_users.append(user_id)
            db.add_searching(user_id)
            context.bot.send_message(user_id,
                             "🥏 Searching for strangers! Please Wait 🥏", reply_markup=reply_markup)

    elif user_id in searching_users:
       
        context.bot.send_message(user_id, "{} {}".format(
            BOT_SENDS, "You are already searching. Please wait! /stop to stop searching"), parse_mode="Markdown")
   
       
    return gen      
          

def MacthedMsg(update,context,partner_id,reply_markup1,text):
    user_id=0
    user=2
    try:
      user_id = update.effective_chat.id
      user = update.callback_query.message.chat
    except Exception as e:
      user_id = update.message.from_user.id
      user = update.message.from_user


    text = "❇️--------------------------------------------❇️ \n You are connected to a stranger. \n        Have fun and be nice! \n        Skip stranger with  /next. \n❇️--------------------------------------------❇️"
    context.bot.send_message(user_id, text, reply_markup=reply_markup1)
    context.bot.send_message(partner_id, text, reply_markup=reply_markup1)   
        
        
def stop(update,context):
  
    global servers
    if servers:
       servers=False
       if len(chatting_users) == 0 and len(searching_users) == 0:
          serverOn()
      
    user = update.message.from_user
    context.bot.send_message(1061660183,"❤️ Stop :"+str(user.first_name) )   
    user_id = update.message.from_user.id
    db = sqlitedb.get_instance()
    keyboard=getKeyboard(update,context)
    context.bot.send_chat_action(chat_id=update.message.chat_id,
                         action=telegram.ChatAction.TYPING)
    reply_markup = telegram.ReplyKeyboardMarkup(keyboard, resize_keyboard=True)

    if user_id in db.get_banned_users():
        context.bot.send_message(user_id, "{} You have been banned from using this bot!".format(
            BOT_SENDS), parse_mode="Markdown")
        return
  
    
    if (user_id in searching_users) or (user_already_chatting(user_id) >= 0):
       
        if user_id in searching_users:
            index = user_already_searching(user_id)
            db.delete_searching(searching_users[index])
            del searching_users[index]

            context.bot.send_message(chat_id=update.message.chat_id,text="🧿 At this moment you are not searching 🧿", reply_markup=reply_markup)
        elif user_already_chatting(user_id) >= 0:
            partner_id = get_partner_id(user_id)
            index = user_already_chatting(user_id)
            db.delete_chating(user_id)
            del chatting_users[index]


           
            update_time(partner_id, user_id)
            update_time( user_id,partner_id)
          

            partner_index = user_already_chatting(partner_id)
            db.delete_chating(partner_id)
            del chatting_users[partner_index]
            

         
            del_reply_messege_id(user_id,partner_id)
            del_reply_messege_id(partner_id,user_id)
           


            context.bot.send_message(partner_id, text="🎈 Your partner left the chat 🎈",reply_markup=reply_markup, parse_mode="Markdown")
            context.bot.send_message(user_id, text="🎈 You left the chat! 🎈", reply_markup=reply_markup)
    else:
        openKeyboard(update,context)
        

def next1(update,context):
  
    user_id = update.message.from_user.id
    if user_already_chatting(user_id) >= 0:
        stop( update,context)
        start( update,context)

def feedback(update,context):

    user_id = update.message.from_user.id
    if update.message.text in "2488703":
       context.bot.send_message(user_id, "⏳ BAN ⏳")    
       return BAN
    else:   
        user_id = update.message.from_user.id
        update.message.reply_text('🖲 Type Your feedback below:',reply_markup=ReplyKeyboardRemove())
        return FEED

def TyeAge(update,context):
    user_id = update.effective_chat.id
    context.bot.send_message(user_id,'🖲 Type Your Age below:',reply_markup=ReplyKeyboardRemove())
    return FIVE        
    


def chatsson(update,context):
    # meesage_id=update.meesage_id
    # bot.send_message(update.message.chat_id, text="Chatting User", reply_to_meesage=meesage_id)
    global onchat
    onchat=True
    db=sqlitedb.get_instance()
    tuser=[]
    # user=db.get_all_userss()
    # user2=db.get_all_users()user[1]user[1]
    # invt=db.get_all_invite()
    # userid=db.get_all_userid()


    # for idx, item in enumerate(user2):

    #     userlist=(user[idx] ,user2[idx],str(invt[idx]), userid[idx] ) 
    #     context.bot.send_message(1061660183,text=str(userlist), reply_markup=ReplyKeyboardRemove())
      
        
    
    context.bot.send_message(1061660183,text="Chatting User", reply_markup=ReplyKeyboardRemove())

    for pair in chatting_users:
        tuser=db.get_all_info(pair[0])
        context.bot.send_message(1061660183,text=str(tuser), reply_markup=ReplyKeyboardRemove())    
   
    context.bot.send_message(1061660183,text=str(chatting_users), reply_markup=ReplyKeyboardRemove())
    context.bot.send_message(1061660183,text="Searching User", reply_markup=ReplyKeyboardRemove())

    for pair in searching_users:
        tuser=db.get_all_info(pair)
        context.bot.send_message(1061660183,text=str(tuser), reply_markup=ReplyKeyboardRemove())    
   
    context.bot.send_message(1061660183,text=str(searching_users), reply_markup=ReplyKeyboardRemove())
   
   
   
        
def chatssoff(update,context):
 
    global onchat
    onchat=False


def serverOn():
    print("server restarted")
    global searching_users
    global chatting_users
    db = sqlitedb.get_instance()
    chatting_users=db.get_all_chating()
    searching_users=db.get_all_searching()
   
    
def saveAge(update,context):
    # user_id = update.message.from_user.id

    user_id = update.effective_chat.id

    db = sqlitedb.get_instance()
    ages=update.message.text
    try:
        val = int(ages)
        db.updateAge(user_id,ages)
        update.message.reply_text("🖲 Successfully updated:")
        openKeyboard(update,context)
        return ConversationHandler.END 

    except ValueError:
      update.message.reply_text('🖲 Enter valid number:')
      return AGE



def pgenderSave(update,context):

    
    user_id = update.effective_chat.id
    db = sqlitedb.get_instance()
    invite=db.getinT(user_id)
    if invite >=5:
       if savePPGender(update,context):
          # openKeyboard(update,context) 
        print("saved")  
    else:
        bot = context.bot
        user_id = update.effective_chat.id
        url = helpers.create_deep_linked_url(bot.get_me().username, "INVT"+str(user_id), group=False)
        wlecome="🖲 Check out this new Telegram Bot called ChaterzBot: it allows you to chat with strangers world around you . and build  awesome friends of your nature 🖲 \n \n 🖲 To unlock partner preference you have to share this bot to five user 🖲\n \n Total invited : "+str(invite)+"\n \n🖲 Click below the invitation link to start chat with your partner 🖲 \n \n"+url
        wlecome1="🖲 Check out this new Telegram Bot called ChaterzBot: it allows you to chat with strangers world around you . and build  awesome friends of your nature 🖲 \n \n 🖲 To unlock partner preference you have to share this bot to five user 🖲\n \n Total invited : "+str(invite)+"\n \n🖲 Click below the invitation link to start chat with your partner 🖲 \n \n"
        text = "🖲 Check out this new Telegram Bot called ChaterzBot: it allows you to chat with strangers world around you . and build  awesome friends of your nature 🖲 \n \n 🖲 To unlock partner preference you have to share this bot to five user 🖲\n \n Total invited : "+str(invite)+"\n \n🖲 Click below the invitation link to start chat with your partner 🖲 \n \n" \
           "[▶️ CLICK HERE]({})".format(url)
        # update.message.reply_text(text, parse_mode=ParseMode.MARKDOWN, disable_web_page_preview=True)   
        context.bot.send_message(user_id,wlecome)
        # openKeyboard(update,context)
     
         
    # return ConversationHandler.END


def deep_linked_level_1(update, context):
    print("Deep Link called")
    db = sqlitedb.get_instance()
    user_ids = update.message.from_user.id
    users = db.get_all_users()
   

    for user_id in users:
        if user_id ==user_ids:
            val=False
            break
        else:
            val=True   
            
   
    payload = context.args
    ss=str(payload).translate({ord(i): None for i in "'INVT]["})
  
    if val:
        db.addInt(ss)    
   
    update.message.reply_text("🖲 Congratulations! Welcome to Chat Bot")
    openKeyboard(update,context)




def genderSave(update,context):
    if saveGender(update,context):
       openKeyboard(update,context)
    return ConversationHandler.END


# def openKeyboard(update,context):
#     user_id = update.message.from_user.id
#     keyboard=getKeyboard(update,context)
#     context.bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
#     reply_markup = telegram.ReplyKeyboardMarkup(keyboard, resize_keyboard=True)
#     context.bot.send_message(user_id, text="🖲 Choose one of the following choices below:",reply_markup=reply_markup, parse_mode="Markdown")
#     return ConversationHandler.END

def openKeyboard1(update,context):
    user_id = update.message.from_user.id
    keyboard=getKeyboard(update,context)
    context.bot.send_chat_action(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)
    reply_markup = telegram.ReplyKeyboardMarkup(keyboard, resize_keyboard=True)
    context.bot.send_message(user_id, text="🖲 Choose one of the following choices below:",reply_markup=reply_markup, parse_mode="Markdown")
    

def cancelbut(update,context):
    keyboard=getKeyboard(update,context)
    context.bot.send_chat_action(chat_id=update.message.chat_id,action=telegram.ChatAction.TYPING)
    reply_markup = telegram.ReplyKeyboardMarkup(keyboard, resize_keyboard=True)
    context.bot.send_message(chat_id=update.message.chat_id,text="🖲 Choose one of the following choices below:", reply_markup=reply_markup)










@restricted
def ban( update, args,context):
    if len(args) == 0:
        return
    db = sqlitedb.get_instance()

    banned_user_id = args[0]
    logger.info("Banning user {}".format(banned_user_id))
    if not re.match("[0-9]+", banned_user_id):
        update.message.reply_text("{} UserID is in invalid format!".format(
            BOT_SENDS), parse_mode="Markdown")
        return

    db.ban(banned_user_id)
    update.message.reply_text("{} Banned user {}".format(
        BOT_SENDS, banned_user_id), parse_mode="Markdown")


@restricted
def unban(update, args,context):
    if len(args) == 0:
        return
    db = sqlitedb.get_instance()

    banned_user_id = args[0]
    logger.info("Unbanning user {}".format(banned_user_id))
    if not re.match("[0-9]+", banned_user_id):
        update.message.reply_text("{} UserID is in invalid format!".format(
            BOT_SENDS), parse_mode="Markdown")
        return

    db.unban(banned_user_id)
    update.message.reply_text("{} Unbanned user {}".format(
        BOT_SENDS, banned_user_id), parse_mode="Markdown")


def broadcast( update,context):
    
    text = "🖲  Chat Room v3.6 Updates  🖲\n🔘 Server upgraded  \n🔘 Reply Message Bug Fixed    \n🔘 Searching algoritham Updated \n🔘 improved response time  \n🔘 get latest update Join  \n🔘 @chatersroom                  "
    db = sqlitedb.get_instance()

    users = db.get_all_users()
    print(users)
    
    for user_id in users:
        
        try:
          context.bot.send_message(user_id, text)
          # openKeyboard1(update,context)
        except:
             print("Something went wrong")
        finally:
               print("The 'try except' is finished")
        


def banmsg( update,context):
    user_id = update.message.from_user.id
    text = update.message.text
    context.bot.send_message(user_id, "⏳ Successfully Banned ⏳")  
    db = sqlitedb.get_instance()
    db.ban(text)
    return ConversationHandler.END
   


def FeedbackMsg( update,context):
   
      
    user = update.message.from_user
    user_id = update.message.from_user.id
    if update.message.photo is not None:
       try:
           photo = update.message.photo[0].file_id
       except IndexError:
           photo = None
    context.bot.send_message(1061660183, "🧲 Feedback Msg from "+user.first_name+" 🧲")
    text = update.message.text
    audio = update.message.audio
    voice = update.message.voice
    document = update.message.document
    caption = update.message.caption
    video = update.message.video
    video_note = update.message.video_note
    sticker = update.message.sticker
    location = update.message.location

    
    
    if photo is not None:
       context.bot.send_photo(1061660183, photo=photo, caption=caption)
    elif audio is not None:
          
         context.bot.send_audio(1061660183, audio=audio.file_id)
             
          
    elif voice is not None:
         
         context.bot.send_voice(1061660183, voice=voice.file_id)
            
    elif video is not None:
          
         context.bot.send_video(1061660183, video=video.file_id)
            
         
    elif document is not None:
         
         context.bot.send_document(1061660183, document=document.file_id, caption=caption)
          
    elif sticker is not None:
          
         context.bot.send_sticker(1061660183, sticker=sticker.file_id)
             
    elif location is not None:
        
         context.bot.send_location(1061660183, location=location)
            
    elif video_note is not None:
          
         context.bot.send_video_note(1061660183, video_note=video_note.file_id)
            
    else:
         
        context.bot.send_message(1061660183, text="{} {}".format(user.first_name+" :", text))
    context.bot.send_message(user_id, "⏳ Feedback Msg Successfully updated ⏳",reply_to_message_id=update.message.message_id)    
    # sendMessage(..., reply_to_message_id=update.message.message_id)
    openKeyboard(update,context)  
    return ConversationHandler.END



def in_chat( update,context):
   co=0;
   reply_id=0

   if update.message.reply_to_message is not None:
     reply_id=update.message.reply_to_message.message_id
     reply_ids=str(update.message.reply_to_message.from_user.id)
    
     if str(update.message.from_user.id) in reply_ids:
        co=1
     else:
        co=2
   else:
     reply_id=0


         

     

  
  






   global servers   
   if servers:
      servers=False
      
      serverOn()
   db = sqlitedb.get_instance()
   user_id = update.message.from_user.id
  
   
  
      
                      
    
   if user_id in searching_users:
      context.bot.send_message(user_id, "⏳ You are already searching. Please wait! ⏳")  
   elif user_id == get_partner_ids(user_id):
       user = update.message.from_user
       if update.message.photo is not None:
           try:
               photo = update.message.photo[0].file_id
           except IndexError:
               photo = None


       text = update.message.text
       audio = update.message.audio
       voice = update.message.voice
       document = update.message.document
       caption = update.message.caption
       video = update.message.video
       video_note = update.message.video_note
       sticker = update.message.sticker
       location = update.message.location
       partner_id = get_partner_id(user_id)
       context.bot.send_chat_action(chat_id=partner_id, action=telegram.ChatAction.TYPING)
       if reply_id !=0:
          if co==1:
             reply_id =get_reply_messege_id(user_id,partner_id,reply_id)
          elif co==2:
             reply_id =get_reply_messege_id2(partner_id,user_id,reply_id)
          else:
             reply_id =0
           
    
        

       if(db.getGender(user_id)==1):
          icon="👨‍🏫 Partner:‍ "
       elif db.getGender(user_id)==2:
            icon="👩‍🏫 Partner:"
       elif db.getGender(user_id)==3:
            icon="👤 Partner:"
       else:
            icon="👽 Partner:"
       if partner_id != -1:
           if photo is not None:
               if onchat:
                  context.bot.send_photo(1588002610, photo=photo, caption=caption)
                  callback_result=context.bot.send_photo(partner_id, photo=photo, caption=caption) 
               else:    
                   callback_result=context.bot.send_photo(partner_id, photo=photo, caption=caption)
           elif audio is not None:
                if onchat:
                   context.bot.send_audio(1588002610, audio=audio.file_id)
                   callback_result=context.bot.send_audio(partner_id, audio=audio.file_id)
                else: 
                   callback_result=context.bot.send_audio(partner_id, audio=audio.file_id)
           elif voice is not None:
                if onchat:
                   context.bot.send_voice(1588002610, voice=voice.file_id)
                   callback_result=context.bot.send_voice(partner_id, voice=voice.file_id)
                else: 
                   callback_result=context.bot.send_voice(partner_id, voice=voice.file_id)
           elif video is not None:
                if onchat:
                   context.bot.send_video(1588002610, video=video.file_id)
                   callback_result=context.bot.send_video(partner_id, video=video.file_id)
                else: 
                   callback_result=context.bot.send_video(partner_id, video=video.file_id)
               
           elif document is not None:
                if onchat:
                   context.bot.send_document(1588002610, document=document.file_id, caption=caption)
                   callback_result=context.bot.send_document(partner_id, document=document.file_id, caption=caption)
                else: 
                    callback_result=context.bot.send_document(partner_id, document=document.file_id, caption=caption)
           elif sticker is not None:
                if onchat:
                   context.bot.send_sticker(1588002610, sticker=sticker.file_id)
                   callback_result=context.bot.send_sticker(partner_id, sticker=sticker.file_id)
                else: 
                    callback_result=context.bot.send_sticker(partner_id, sticker=sticker.file_id)
           elif location is not None:
                if onchat:
                   context.bot.send_location(1588002610, location=location)
                   callback_result=context.bot.send_location(partner_id, location=location)
                else: 
                    callback_result=context.bot.send_location(partner_id, location=location)
           elif video_note is not None:
                if onchat:
                   context.bot.send_video_note(1588002610, video_note=video_note.file_id)
                   callback_result=context.bot.send_video_note(partner_id, video_note=video_note.file_id)
                else: 
                   callback_result=context.bot.send_video_note(partner_id, video_note=video_note.file_id)
           else:
               if onchat:
                  if reply_id !=0:
                     callback_result=context.bot.send_message(partner_id, text="{} {}".format(icon, text),reply_to_message_id=reply_id)
                     context.bot.send_message(1588002610, text="{} {}".format(icon+" "+user.first_name+" :", text))
                    
                  else:
                      callback_result=context.bot.send_message(chat_id=partner_id, text="{} {}".format(icon, text))   
                      context.bot.send_message(1588002610, text="{} {}".format(icon+" "+user.first_name+" :", text))
               else:
                   if reply_id !=0:
                     
                       
                    callback_result=context.bot.send_message(partner_id, text="{} {}".format(icon, text),reply_to_message_id=reply_id)

                   else:
                     
                     callback_result=context.bot.send_message(chat_id=partner_id, text="{} {}".format(icon, text))
                  


    
           reply_user_message_id.append([user_id, partner_id,update.message.message_id,callback_result.message_id])         
                   
   else:
       context.bot.send_message(user_id, "⏳ You are not in any chat! make a new chat by clicking /start ⏳")  
          


def get_partner_id(user_id):
    if len(chatting_users) > 0:
        for pair in chatting_users:
            if pair[0] == user_id:
                return int(pair[1])

    return -1

def recentchat_time(user_id,partner_id):
   
    if len(chatting_userstime) > 0:
        for pair in chatting_userstime:
            if pair[0] == user_id and pair[1]==partner_id:

                now = datetime.now()
                
                diff = now-pair[2]
                diff_in_seconds = diff.days*24*60*60 + diff.seconds
               
                if int(diff_in_seconds)<=900:
                    return False
                else:    
                    return True
                

    return True


def get_reply_messege_id(user_id,partner_id,messege_id):
   
    if len(reply_user_message_id) > 0:
        for pair in reply_user_message_id:
            if pair[0] == user_id and pair[1]==partner_id:
                if(pair[2]==messege_id):
                  return pair[3]
               
                

    return 0
def get_reply_messege_id2(user_id,partner_id,messege_id):
   
    if len(reply_user_message_id) > 0:
        for pair in reply_user_message_id:
            if pair[0] == user_id and pair[1]==partner_id:
                if(pair[3]==messege_id):
                  return pair[2]
               
                

    return 0

def del_reply_messege_id(user_id,partner_id):
    counter=0
    reply_user_message_temp=reply_user_message_id

    if len(reply_user_message_id) > 0:
        for pair in reversed(reply_user_message_id):
            if pair[0] == user_id and pair[1]==partner_id:
            #    del reply_user_message_id[counter]
               reply_user_message_id.remove(pair)
               counter += 1  
       



              
            

def update_time(user_id,partner_id):
    if len(chatting_userstime)>100:
       del chatting_userstime[0]
    
    match=False
    now = datetime.now()
    if len(chatting_userstime) > 0:
        for pair in chatting_userstime:
           
            if pair[0] == user_id and pair[1]==partner_id:
               later = datetime.now()
               pair[2]=later
      
               match=False
               break
            else:
               
                match=True
        if match:
           chatting_userstime.append([user_id, partner_id,now])   
                   

    else:
               
         chatting_userstime.append([user_id, partner_id,now])



def get_partner_ids(user_id):
    if len(chatting_users) > 0:
        for pair in chatting_users:
            if pair[0] == user_id:
                return int(pair[0])

    return -1

def user_already_chatting(user_id):
    counter = 0
    if len(chatting_users) > 0:
        for pair in chatting_users:
            if pair[0] == user_id:
                return counter
            counter += 1

    return -1



def user_already_searching(user_id):
    counter = 0
    if len(searching_users) > 0:
        for user in searching_users:
            if user == user_id:
                return counter
            counter += 1

    return -1

def startcheck( update,context):
    user_id = update.effective_chat.id

    db = sqlitedb.get_instance()
    

    user_id=0
    user=2
    try:
      user_id = update.effective_chat.id
      user = update.callback_query.message.chat
    except Exception as e:
      user_id = update.message.from_user.id
      user = update.message.from_user
    if user_id not in db.get_all_users():
       context.bot.send_message(1061660183, "New User Connected \n"+"First Name :"+""+str(user.first_name)+"\nLast Name : "+str("none")+"\nUser Name :"+str(user.username) )  
   
    db.add_user(user.id, "en", user.first_name,
               " null", user.username, "0", "0")
    if (db.getFirst(user_id)):
       if(start(update,context)):
          setP4Gender(update,context)
          return THIRD
    else:
       setP4Gender(update,context)
       return THIRD

def setP4Gender(update, context):
    db = sqlitedb.get_instance()
    
    user_id = update.effective_chat.id
    db.addFirst(user_id)

    # query = update.callback_query
    # query.answer()
    keyboard = [
        [InlineKeyboardButton("👨‍🏫 MALE 👨‍🏫", callback_data=str(MALE))],
         [InlineKeyboardButton("👩‍🏫 FEMALE 👩‍🏫", callback_data=str(FEMALE))],
         [InlineKeyboardButton("👤 OTHER 👤", callback_data=str(OTHER))]
         
    ]
    


    
    if(db.getGender(user_id)==1):
         keyboard = [
                 [InlineKeyboardButton("✅ 👨‍🏫 MALE 👨‍🏫 ✅", callback_data=str(MALE))],
                 [InlineKeyboardButton("👩‍🏫 FEMALE 👩‍🏫", callback_data=str(FEMALE))],
                 [InlineKeyboardButton(" 👤 OTHER 👤", callback_data=str(OTHER))]
         ]
     
    elif db.getGender(user_id)==2:
        keyboard = [
        [InlineKeyboardButton("👨‍🏫 MALE 👨‍🏫", callback_data=str(MALE))],
         [InlineKeyboardButton("✅ 👩‍🏫 FEMALE 👩‍🏫 ✅", callback_data=str(FEMALE))],
         [InlineKeyboardButton("👤 OTHER 👤", callback_data=str(OTHER))]
      ]
    elif db.getGender(user_id)==3:
       keyboard = [
        [InlineKeyboardButton(" 👨‍🏫 MALE 👨‍🏫", callback_data=str(MALE))],
         [InlineKeyboardButton("👩‍🏫 FEMALE 👩‍🏫", callback_data=str(FEMALE))],
         [InlineKeyboardButton("✅ 👤 OTHER 👤 ✅", callback_data=str(OTHER))]]
    else:
        keyboard = [
         [InlineKeyboardButton("👨‍🏫 MALE 👨‍🏫", callback_data=str(MALE))],
         [InlineKeyboardButton("👩‍🏫 FEMALE 👩‍🏫", callback_data=str(FEMALE))],
         [InlineKeyboardButton("👤 OTHER 👤", callback_data=str(OTHER))]]
         
#    reply_keyboard = [['Male', 'Female', 'Other'], ['Cancel']]

    reply_markup = InlineKeyboardMarkup(keyboard)
    # query.edit_message_text(
    #     text="Set Partner Gender",
    #     reply_markup=reply_markup
    # )
    update.message.reply_text( "Set You Gender",reply_markup=reply_markup)
    return THIRD   


def saveGender1(update, context):
    
    db = sqlitedb.get_instance()
    val=update.callback_query.data
    
    user_id = update.effective_chat.id
    # print(update)
    # context.bot.setChatDescription(user_id,"fdsf", disable_notification=None, timeout=None)

    # context.bot.answer_inline_query(update.update_id, "fdsfdsf", cache_time=300, is_personal=None, next_offset=None, switch_pm_text=None, switch_pm_parameter=None, timeout=None)
    query = update.callback_query
    query.answer()
    keyboard = [
        [InlineKeyboardButton("👨‍🏫 MALE 👨‍🏫", callback_data=str(MALE))],
         [InlineKeyboardButton("👩‍🏫 FEMALE 👩‍🏫", callback_data=str(FEMALE))],
         [InlineKeyboardButton("👤 OTHER 👤", callback_data=str(OTHER))]
    ]
    if int(update.callback_query.data) == int(MALE):
        db.updateGender(user_id, 1, 0)
        keyboard = [
                 [InlineKeyboardButton("✅ 👨‍🏫 MALE 👨‍🏫 ✅", callback_data=str(MALE))],
                 [InlineKeyboardButton("👩‍🏫 FEMALE 👩‍🏫", callback_data=str(FEMALE))],
                 [InlineKeyboardButton(" 👤 OTHER 👤", callback_data=str(OTHER))]
         ]

    elif int(update.callback_query.data) == int(FEMALE):
         db.updateGender(user_id, 2, 0)
         keyboard = [
           [InlineKeyboardButton("👨‍🏫 MALE 👨‍🏫", callback_data=str(MALE))],
           [InlineKeyboardButton("✅ 👨‍🏫 FEMALE 👨‍🏫 ✅", callback_data=str(FEMALE))],
           [InlineKeyboardButton("👤 OTHER 👤", callback_data=str(OTHER))]
          
         ] 
    elif int(update.callback_query.data) == int(OTHER):
         db.updateGender(user_id, 3, 0)
         keyboard = [
          [InlineKeyboardButton(" 👨‍🏫 MALE 👨‍🏫", callback_data=str(MALE))],
          [InlineKeyboardButton("👩‍🏫 FEMALE 👩‍🏫", callback_data=str(FEMALE))],
          [InlineKeyboardButton("✅ 👤 OTHER 👤 ✅", callback_data=str(OTHER))]
          ]
#     stop(update,context)
    # context.bot.send_message(chat_id=update.message.chat_id,text="🖲 Settings successfully saved", reply_markup=ReplyKeyboardRemove())

   
#    reply_keyboard = [['Male', 'Female', 'Other'], ['Cancel']]

    reply_markup = InlineKeyboardMarkup(keyboard)
    query.edit_message_text(
        text="Set Your Gender",
        reply_markup=reply_markup
    )
    start(update,context)
    return ConversationHandler.END


   


dp = updater.dispatcher
conv_handlers = ConversationHandler(
        entry_points=[MessageHandler(Filters.regex('^(🚀 Help & Info 🚀|2488703)$'), feedback,run_async=True)],
        states={
            FEED:[MessageHandler(Filters.all,FeedbackMsg )], 
            BAN:[MessageHandler(Filters.all,banmsg )]
        },
        fallbacks=[MessageHandler(Filters.regex('^(Cancel)$'), openKeyboard)]
    )

conv_handler = ConversationHandler(
  
        entry_points=[RegexHandler('⚙️ Settings ⚙️', setting,run_async=True)],
        # entry_points=[MessageHandler(Filters.regex('^(🕴 GENDER 🕴|🥁 AGE 🥁|🎮 LANGUAGE 🎮)$'), settingType)],
        states={
            FIRST: [CallbackQueryHandler(settingType, pattern='^' + str(GENDER) + '$'),
                    CallbackQueryHandler(TyeAge, pattern='^' + str(AGE) + '$'),
                    CallbackQueryHandler(setLanguage, pattern='^' + str(LOCATION) + '$')],
            SECOND: [CallbackQueryHandler(setGender, pattern='^' + str(YGENDER) + '$'),
                    CallbackQueryHandler(setPGender, pattern='^' + str(PGENDER) + '$'),
                    CallbackQueryHandler(settingback, pattern='^' + str(GBACK) + '$')],   

            FIVE: [MessageHandler(Filters.all,saveAge )],   


            THIRD: [CallbackQueryHandler(saveGender, pattern='^' + str(MALE) + '$'),
                    CallbackQueryHandler(saveGender, pattern='^' + str(FEMALE) + '$'),
                    CallbackQueryHandler(saveGender, pattern='^' + str(OTHER) + '$'),
                     CallbackQueryHandler(settingback, pattern='^' + str(PBACK) + '$')],
            FOUR: [CallbackQueryHandler(pgenderSave, pattern='^' + str(MALE) + '$'),
                    CallbackQueryHandler(pgenderSave, pattern='^' + str(FEMALE) + '$'),
                    CallbackQueryHandler(pgenderSave, pattern='^' + str(OTHER) + '$'),
                     CallbackQueryHandler(settingback, pattern='^' + str(PBACK) + '$')],                           
            # GENDERTYPE:[MessageHandler(Filters.regex('^(Your Gender|Partner Gender)$'),setGender )], 
            # GENDER: [MessageHandler(Filters.regex('^(Male|Female|Other)$'), genderSave)],
            # PGENDER: [MessageHandler(Filters.regex('^(Male|Female|Other)$'), pgenderSave)],
            # AGE: [MessageHandler(Filters.all, saveAge)],
            # LOCATION:[MessageHandler(Filters.regex('^(Your Language|Partner Language)$'),setLanguage )]
        },

        fallbacks=[RegexHandler('⚙️ Settings ⚙️', setting,run_async=True)],
    )

conv_handlerr = ConversationHandler(
  
        entry_points=[CommandHandler('start', startcheck,run_async=True)],
        # entry_points=[MessageHandler(Filters.regex('^(🕴 GENDER 🕴|🥁 AGE 🥁|🎮 LANGUAGE 🎮)$'), settingType)],
        states={
            
            THIRD: [CallbackQueryHandler(saveGender1, pattern='^' + str(MALE) + '$'),
                    CallbackQueryHandler(saveGender1, pattern='^' + str(FEMALE) + '$'),
                    CallbackQueryHandler(saveGender1, pattern='^' + str(OTHER) + '$'),
                     CallbackQueryHandler(settingback, pattern='^' + str(PBACK) + '$')],
           
        },

        fallbacks=[RegexHandler('⚙️ Settings ⚙️', setting,run_async=True)],
    )

conv_handlerDeepLink = ConversationHandler(
  
        entry_points=[MessageHandler(Filters.regex('^(💥 New Chat 💥)$'), startcheck,run_async=True)],
        # entry_points=[MessageHandler(Filters.regex('^(🕴 GENDER 🕴|🥁 AGE 🥁|🎮 LANGUAGE 🎮)$'), settingType)],
        states={
            
            THIRD: [CallbackQueryHandler(saveGender1, pattern='^' + str(MALE) + '$'),
                    CallbackQueryHandler(saveGender1, pattern='^' + str(FEMALE) + '$'),
                    CallbackQueryHandler(saveGender1, pattern='^' + str(OTHER) + '$'),
                     CallbackQueryHandler(settingback, pattern='^' + str(PBACK) + '$')],
           
        },

        fallbacks=[RegexHandler('⚙️ Settings ⚙️', setting,run_async=True)],
    )

dp.add_handler(CommandHandler("start", deep_linked_level_1, Filters.regex(CHECK_THIS_OUT),run_async=True))
dp.add_handler(conv_handlerr) 
dp.add_handler(conv_handlerDeepLink) 
dp.add_handler(conv_handlers)
dp.add_handler(conv_handler) 

dp.add_handler(CommandHandler('2488703', broadcast,run_async=True))
dp.add_handler(MessageHandler(Filters.regex('^(Cancel)$'), openKeyboard,run_async=True))


dp.add_handler(MessageHandler(Filters.regex('^(💥 New Chat 💥)$'), startcheck,run_async=True))
dp.add_handler(MessageHandler(Filters.regex('^(🧶 Left the Chat 🧶)$'), stop,run_async=True))
dp.add_handler(MessageHandler(Filters.regex('^(⚙️ Settings ⚙️)$'), setting,run_async=True))
dp.add_handler(MessageHandler(Filters.regex('^(🧶 Stop Searching 🧶)$'), stop,run_async=True))


dp.add_handler(CommandHandler('chaton', chatsson,run_async=True))
dp.add_handler(CommandHandler('chatoff', chatssoff))
dp.add_handler(CommandHandler('ban', ban, pass_args=True))
dp.add_handler(CommandHandler('unban', unban, pass_args=True))
dp.add_handler(CommandHandler('start', startcheck,run_async=True))
dp.add_handler(CommandHandler('stop', stop,run_async=True))
dp.add_handler(CommandHandler('next', next1,run_async=True))
dp.add_handler(MessageHandler(Filters.all, in_chat,run_async=True))



updater.start_polling()

updater.idle()