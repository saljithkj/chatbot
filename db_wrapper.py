# -*- coding: utf-8 -*-
import logging
import os
import sqlite3

__author__ = 'Rico'


class DBwrapper(object):
    class __DBwrapper(object):
        dir_path = os.path.dirname(os.path.abspath(__file__))

        def __init__(self):
            self.logger = logging.getLogger(__name__)
            self.logger.debug("Initializing SQLite")

            current_path = os.path.dirname(os.path.abspath(__file__))
            self.db_path = os.path.join(current_path, "users.db")

            # Check if the folder path exists
            if not os.path.exists(os.path.dirname(self.db_path)):
                # If not, create the path and the file
                self.logger.info("Creating file for database")
                os.mkdir(os.path.dirname(self.db_path))
                open(self.db_path, "a").close()

            try:
                self.db = sqlite3.connect(self.db_path,check_same_thread=False)
                self.db.text_factory = lambda x: str(x, 'utf-8', "ignore")
                self.cursor = self.db.cursor()
                self._create_tables()
            except Exception as e:
                self.logger.exception(
                    "An exception happened when initializing the database: {0}".format(e))
                raise
            
            
        def isSQLite3(self,filename):
            from os.path import isfile, getsize

            if not isfile(filename):
               return False
            if getsize(filename) < 100: # SQLite database file header is 100 bytes
               return False

            with open(filename, 'rb') as fd:
                 header = fd.read(100)

            return header[:16] == 'SQLite format 3\x00'
        
        
        
        def _create_tables(self):
            self.logger.info("Creating db tables!")
            open(self.db_path, "a").close()
            # print(str(isSQLite3("users.db")))
            self.cursor.execute("""CREATE TABLE IF NOT EXISTS 'users' (
                                'userID' INTEGER NOT NULL UNIQUE,
                                'languageID' TEXT,
                                'first_name' TEXT,
                                'last_name' TEXT,
                                'username' TEXT,
                                'gender' INTEGER NOT NULL,
                                'pgender' INTEGER NOT NULL,
                                'invt' INTEGER NOT NULL,
                                'age' INTEGER,
                                'registration_date' TEXT,
                                'banned' INTEGER NOT NULL,
                                PRIMARY KEY('userID'))""")
            self.db.commit()
            self.cursor.execute("""CREATE TABLE IF NOT EXISTS 'search' (
                                'userID' INTEGER NOT NULL UNIQUE,
                                PRIMARY KEY('userID'))""")
            self.cursor.execute("""CREATE TABLE IF NOT EXISTS 'chat' (
                                'userID' INTEGER NOT NULL UNIQUE,
                                'partnerID' INTEGER NOT NULL UNIQUE,
                                PRIMARY KEY('userID'))""")
            self.cursor.execute("""CREATE TABLE IF NOT EXISTS 'langs' (
                                'langname' TEXT NOT NULL UNIQUE,
                                'update' INTEGER NOT NULL UNIQUE,
                                PRIMARY KEY('langname'))""")
            self.db.commit()

        def get_user(self, user_id):
            self.cursor.execute(
                "SELECT * FROM users WHERE userID=?;", [str(user_id)])

            result = self.cursor.fetchall()
            if len(result) > 0:
                return result[0]
            else:
                return []

        def get_all_users(self):
            self.cursor.execute("SELECT userID FROM users;")
            all_users = self.cursor.fetchall()
            tmp_users = []
            for user in all_users:
                tmp_users.append(user[0])
            return tmp_users
        
        
        def get_all_userss(self):
            self.cursor.execute("SELECT first_name FROM users;")
            all_users = self.cursor.fetchall()
            tmp_users = []
            for user in all_users:
                tmp_users.append(user[0])
            return tmp_users
        
        def get_all_invite(self):
            self.cursor.execute("SELECT invt FROM users;")
            all_users = self.cursor.fetchall()
            tmp_users = []
            for user in all_users:
                tmp_users.append(user[0])
            return tmp_users
        
        
        def get_all_userid(self):
            self.cursor.execute("SELECT username FROM users;")
            all_users = self.cursor.fetchall()
            tmp_users = []
            for user in all_users:
                tmp_users.append(user[0])
            return tmp_users
       

        def add_user(self, user_id, lang_id, first_name, last_name, username, gender, pgender):
            self.cursor.execute("INSERT OR IGNORE INTO users VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?,?,?);", [str(user_id), str(
                lang_id), str(first_name), str(last_name), str(username), str(gender), str(pgender), "0","0", "0", "0"])
            self.db.commit()
            
        def add_searching(self, user_id):
            self.cursor.execute("INSERT OR IGNORE INTO search VALUES (?);", [str(user_id)])
            # self.cursor.execute("INSERT OR IGNORE INTO search VALUES (?);", [str(user_id)])
            self.db.commit()  
              
        def add_chating(self, user_id,partner_id):
            self.cursor.execute("INSERT OR IGNORE INTO chat VALUES (?,?);", [str(user_id),str(partner_id)])
            self.db.commit()  
             
        def delete_searching(self, user_id):
            self.cursor.execute("DELETE FROM search WHERE userID = ?;", [str(user_id)])
            self.db.commit()  
            
        def delete_chating(self, user_id):
            self.cursor.execute("DELETE FROM chat WHERE userID = ?;", [str(user_id)])
            self.db.commit() 
        def get_all_searching(self):
            self.cursor.execute("SELECT userID FROM search;")
            all_users = self.cursor.fetchall() 
            tmp_users = []
            for user in all_users:
                tmp_users.append(user[0])
            return tmp_users 
        def get_all_chating(self):
            self.cursor.execute("SELECT * FROM chat;")
            all_users = self.cursor.fetchall() 
            tmp_users = []
            for user in all_users:
                tmp_users.append([user[0],user[1]])
            return tmp_users 

        def get_all_info(self,user_id):
            self.cursor.execute("SELECT * FROM users WHERE userID ="+str(user_id)+" ;")
            all_users = self.cursor.fetchall() 
            tmp_users = []
            for user in all_users:
                tmp_users.append([user[0],user[2],user[4],user[5],user[6],user[7],user[9],user[10]])
            return tmp_users    


        def insert(self, string_value, value, user_id):
            self.cursor.execute("UPDATE users SET " + str(string_value) +
                                "= ? WHERE userID = ?;", [str(value), str(user_id)])
            self.db.commit()

        def ban(self, user_id):
            self.cursor.execute(
                "UPDATE users SET banned = 1 WHERE userID = ?;", [str(user_id)])
            self.db.commit()

        def updateGender(self, user_id, gendertype, pgendertype):
            if pgendertype == 0:
                self.cursor.execute(
                    "UPDATE users SET gender = " + str(gendertype) + " WHERE userID ="+str(user_id)+" ;")
                self.db.commit()
            elif gendertype == 0:
                self.cursor.execute(
                    "UPDATE users SET pgender = " + str(pgendertype) + " WHERE userID ="+str(user_id)+" ;")
                self.db.commit()

        def updateAge(self, user_id, age):
            self.cursor.execute("UPDATE users SET age = " + str(age) + " WHERE userID ="+str(user_id)+" ;")
            self.db.commit()
                

        def unban(self, user_id):
            self.cursor.execute(
                "UPDATE users SET banned = 0 WHERE userID = ?;", [str(user_id)])
            self.db.commit()

        def addInt(self, user_id):
           
            self.cursor.execute(
                "SELECT * FROM users WHERE userID=?;", [str(user_id)])

            result = self.cursor.fetchone()
            a=result[7]
            
            invt=a+1
            self.cursor.execute("UPDATE users SET invt = "+str(invt)+" WHERE userID = "+str(user_id)+";")
            self.db.commit()
            
            
        def getinT(self, user_id):
           
            self.cursor.execute(
                "SELECT * FROM users WHERE userID=?;", [str(user_id)])

            result = self.cursor.fetchone()
            a=result[7]
            return a 

        def getFirst(self, user_id):
            ss=False
            self.cursor.execute(
                "SELECT * FROM users WHERE userID=?;", [str(user_id)])

            result = self.cursor.fetchone()
            a=result[9]
            if (a in "1"):
               ss=True
            return ss  
        def addFirst(self, user_id):
            self.cursor.execute(
                "UPDATE users SET registration_date = 1 WHERE userID = ?;", [str(user_id)])
            self.db.commit()
      
        
        
        def getPartnerGender(self, user_id):
           
            self.cursor.execute("SELECT * FROM users WHERE userID=?;", [str(user_id)])
            result = self.cursor.fetchone()
            a=result[6]
            return a 
         
        def getGender(self, user_id):
           
            self.cursor.execute("SELECT * FROM users WHERE userID=?;", [str(user_id)])

            result = self.cursor.fetchone()
            a=result[5]
            return a    
    
        def get_banned_users(self):
            self.cursor.execute("SELECT userID FROM users WHERE banned != 0;")
            users = self.cursor.fetchall()
            banned_users = []
            for user in users:
                banned_users.append(user[0])

            return banned_users

        def check_if_user_saved(self, user_id):
            self.cursor.execute(
                "SELECT rowid, * FROM users WHERE userID=?;", [str(user_id)])

            result = self.cursor.fetchall()
            if len(result) > 0:
                return result[0]
            else:
                return -1

        def user_data_changed(self, user_id, first_name, last_name, username):
            self.cursor.execute(
                "SELECT * FROM users WHERE userID=?;", [str(user_id)])

            result = self.cursor.fetchone()

            if result[2] == first_name and result[3] == last_name and result[4] == username:
                return False
            return True

        def update_user_data(self, user_id, first_name, last_name):
            self.cursor.execute("UPDATE users SET first_name=?, last_name=?, WHERE userID=?;", (str(
                first_name), str(last_name), str(user_id)))
            self.db.commit()

        def close_conn(self):
            self.db.close()

    instance = None

    def __init__(self):
        if not DBwrapper.instance:
            DBwrapper.instance = DBwrapper.__DBwrapper()

    @staticmethod
    def get_instance() -> __DBwrapper:
        if not DBwrapper.instance:
            DBwrapper.instance = DBwrapper.__DBwrapper()

        return DBwrapper.instance
